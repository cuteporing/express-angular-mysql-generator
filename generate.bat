@echo off

:: Do not edit outside of configuration block

:: CONFIGURATION
:: -----------------------------------------------------------------
SET PROJECT_NAME=example-project
:: 
SET PROJECT_LOCATION=C:\
:: -----------------------------------------------------------------


SET GENERATOR_PATH=%~dp0

:: Installing and using Express
:: -----------------------------------------------------------------
:: Need elevation, Installing the Express applicaiton generator, will
:: help create the skeleton for your application.
::npm install express-generator –g

:: Go to project location
cd %PROJECT_LOCATION%

:: Create the skeleton for our application by running the following command the in terminal:
:: Install the dependencies of the template created. 
:: express %PROJECT_NAME% --view=ejs

	:: After running the command, we are going to see a folder with the same name as our application with the following structure:
	::<PROJECT_NAME>/
	::├───app.js
	::├───package.json
	::├───bin/
	::│ └─www
	::├───public/
	::│ ├───images/
	::│ ├───javascripts/
	::│ └───stylesheets/
	::│ └─style.css
	::├───routes/
	::│ ├─index.js
	::│ └─users.js
	::└───views
	::├─error.ejs
	::└─index.ejs

:: Installing AngularJS and Angular-Route
:: -----------------------------------------------------------------
:: npm install angular@1.5.8 angular-route@1.5.8

:: Once the packages are downloaded and installed in the node_modules folder we are 
:: going to copy the folders angular and angular-route inside our public/javascripts folder.
:: Note: to avoid doing this manually you can install Bower, use the CDN for the AngularJS modules, or any other alternative.

:: The folder structure
:: Create two folders in the root path of our application: models and data
:: mkdir models && mkdir data

	:: In the end, our application will have something like the following structure
	::<PROJECT_NAME>/
	::├───app.js
	::├───mysql-connector-nodejs-1.0.4.tar.gz
	::├───package.json
	::├───bin/
	::│ └─www
	::├───data/
	::├───models/
	::├───node_modules/
	::├───public/
	::│ ├───images/
	::│ ├───javascripts/
	::│ └───stylesheets/
	::│ └─style.css
	::├───routes/
	::│ ├─index.js
	::│ └─users.js
	::└───views
	::├─error.ejs
	::└─index.ejs

:: Building the application
:: -----------------------------------------------------------------

:: All commands must be be run using && to be able to perform tasks sequentially
express %PROJECT_NAME% --view=ejs &&  cd %PROJECT_LOCATION%/%PROJECT_NAME% && npm install && copy %GENERATOR_PATH%lib\mysql-connector-nodejs-1.0.5.tar.gz %PROJECT_LOCATION%\%PROJECT_NAME% && npm install angular@1.5.8 angular-route@1.5.8 && mkdir models && mkdir data && start %PROJECT_LOCATION%/%PROJECT_NAME% && npm start

pause > nul