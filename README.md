# express-angular-mysql-generator
## Requirements
- Windows 7 +
- Node.js

## Setup
1. Open **generate.bat** in a text editor.
2. Edit the configuration block

		SET PROJECT_NAME=example-project
    	SET PROJECT_LOCATION=C:\

3. Run **generate.bat** as an Administrator.
4. In the end, our application will have something like the following structure
    This will create a project folder

        example-project/
        ├───app.js
        ├───mysql-connector-nodejs-1.0.4.tar.gz
        ├───package.json
        ├───bin/
        │ └─www
        ├───data/
        ├───models/
        ├───node_modules/
        ├───public/
        │ ├───images/
        │ ├───javascripts/
        │ └───stylesheets/
        │   └─style.css
        ├───routes/
        │ ├─index.js
        │ └─users.js
        └───views
          ├─error.ejs
          └─index.ejs

5. To start express server, simply run the code below
    ```
    npm start
    ```
    **NOTE**: **generate.bat** will also start the server after completing the installation


6. By default your project is accessible through	[http://localhost:3000](http://localhost:3000)

	![](https://gitlab.com/cuteporing/express-angular-mysql-generator/raw/master/img/Express.PNG)


#### Reference:
Check out [InsideMySQL.com](http://insidemysql.com/develop-by-example-document-store-working-with-express-js-angularjs-and-node-js/) for a more detailed explanation.

